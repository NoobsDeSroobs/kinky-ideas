// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "wallPart.generated.h"

/**
 * 
 */
UCLASS()
class OCULUSTEST_API AwallPart : public AActor
{
	GENERATED_BODY()

public:
	AwallPart(const class FObjectInitializer& PCIP);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Actor)
	TSubobjectPtr<UBoxComponent> WallCollisionComponent;
	
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = Actor)
	TSubobjectPtr<UStaticMeshComponent> WallMeshComponent;
	
};

/*


#include "wallPart.h"


UCLASS()
class OCULUSTEST_API ASpawnVolume : public AActor
{
	GENERATED_BODY()

public:
	ASpawnVolume(const class FObjectInitializer& PCIP);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawning)
		TSubobjectPtr<UBoxComponent> WhereToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawning)
		TSubobjectPtr<AwallPart> WhatToSpawn;

	UFUNCTION(BlueprintPure, Category = Spawning)
		FVector getSpawnLocation();

private:
	void SpawnPickup();

};

ASpawnVolume::ASpawnVolume(const class FObjectInitializer& PCIP) : Super(PCIP)
{
	//Create
	WhereToSpawn = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("WallBoxComponent"));
	//Set as root
	RootComponent = WhereToSpawn;
	//Create
	WhatToSpawn = PCIP.CreateDefaultSubobject<AwallPart>(this, TEXT("AwallPart"));
	//Set
	WallMeshComponent->AttachTo(RootComponent);
	//Edit settings
	WallMeshComponent->SetSimulatePhysics(true);
}

FVector getSpawnLocation()
{

}

void ASpawnVolume::SpawnPickup()
{
	if (WhatToSpawn != NULL) {
		UWorld* const World = GetWorld();
		if (World) {
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;
			FVector spawnLocation = getSpawnLocation();

			FRotator SpawnRotation;
			SpawnRotation.Yaw = 0;
			SpawnRotation.Pitch = 0;
			SpawnRotation.Roll = 0;

			AwallPart* const SpawnedWallPart = World->SpawnActor<AwallPart>(WhatToSpawn, SpawnLocation, SpawnRotation);
		}
	}
}

*/