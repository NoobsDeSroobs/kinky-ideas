// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <string>



/**
 * 
 */
class OCULUSTEST_API PointcloudSmoothener
{
public:
	PointcloudSmoothener();
	PointcloudSmoothener(TCHAR* path);
	PointcloudSmoothener(FString path);
	~PointcloudSmoothener();

	TArray<FVector> Points;

	TArray<FVector> smoothByAvarage(TArray<FVector> points);
	TArray<FVector> HalfNumPoints(TArray<FVector> points);

	void BuildSpawnPoints();

	////After we have reduced the pointcloud to its bare minimum, readjust by making all angles orthogonal.
	//std::vector<FVector> restoreOrthogonality(std::vector<FVector> points);

	////Keep updating the base vector as we go until a potensial corner has been hit, then branch and check if said corner exists. 
	////If it does, save the corner and jump to the corner point and restart the algorithm.
	//std::vector<FVector> smoothByJumping(std::vector<FVector> points);

	////A variant of the smooth by average algorithm. This algorithm assumes that all vectors that start from the end of the base vector 
	////and lead to the next point can be a corner and only checks if the angle between these vectors is great enough.
	//std::vector<FVector> smoothByAveragingBaseVector(std::vector<FVector> points);

	////Since the LIDAR can guarantee some form of limit to the inaccuracy of its readings we can use this limit and say that all points 
	////far enough from the base plane is not part of said plane. As such we can assume that the first point in the list to breach this limit is on the new wall.
	////If we can create planes that intersect we can calculate the corners to be where they intersect.
	//std::vector<FVector> smoothByOffsetToPlane(std::vector<FVector> points);

private:
	TArray<FVector> constructPointVectors(TArray<FString> *data);
	FString path;

};
