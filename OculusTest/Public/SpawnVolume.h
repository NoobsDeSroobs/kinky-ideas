// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "wallPart.h"
#include "PointcloudSmoothener.h"
#include "SpawnVolume.generated.h"



/**
*
*/
UCLASS()

class OCULUSTEST_API ASpawnVolume : public AActor
{
	GENERATED_BODY()

public:
	ASpawnVolume(const class FObjectInitializer& PCIP);
	~ASpawnVolume();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawning)
		TSubobjectPtr<UBoxComponent> WhereToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawning)
		TSubclassOf<class AwallPart> WhatToSpawn;

	UFUNCTION(BlueprintPure, Category = Spawning)
		FVector getSpawnLocation();


	virtual void Tick(float DeltaSeconds) override;
private:
	void SpawnObj();
	float width;
	float WallSpacing;
	bool spawned;
	TArray<FVector> Walls;
	int NextCorner;
	PointcloudSmoothener* PSmooth;

	FVector lastSpawnLocation;

};

