// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "OculusTestGameMode.generated.h"

UCLASS(minimalapi)
class AOculusTestGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AOculusTestGameMode(const FObjectInitializer& ObjectInitializer);
};



