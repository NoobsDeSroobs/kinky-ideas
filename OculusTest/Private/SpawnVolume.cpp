// Fill out your copyright notice in the Description page of Project Settings.

#include "OculusTest.h"
#include "SpawnVolume.h"
#include "PointcloudSmoothener.h"


ASpawnVolume::ASpawnVolume(const class FObjectInitializer& PCIP) : Super(PCIP)
{
	//Create
	WhereToSpawn = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("WallBoxComponent"));
	//Set as root
	RootComponent = WhereToSpawn;
	//Create
	//WhatToSpawn = PCIP.CreateDefaultSubobject<AwallPart>(this, TEXT("AwallPart"));

	PrimaryActorTick.bCanEverTick = true;

	spawned = false;
	width = 0.0f;
	FString f = "../point_file.txt";
	PSmooth = new PointcloudSmoothener(f);
	NextCorner = 0;
	WallSpacing = 100.0f;
}

ASpawnVolume::~ASpawnVolume()
{
	delete PSmooth;
}

FVector ASpawnVolume::getSpawnLocation()
{
	if (NextCorner == 0) {
		lastSpawnLocation = Walls[NextCorner];
		return Walls[NextCorner++];
	} else {
		FVector ToNextCorner = Walls[NextCorner] - lastSpawnLocation;
		if (ToNextCorner.Size() > WallSpacing) {
			ToNextCorner *= (WallSpacing / ToNextCorner.Size());
			lastSpawnLocation = lastSpawnLocation+ToNextCorner;
			return lastSpawnLocation;
		} else {
			lastSpawnLocation = Walls[NextCorner];
			return Walls[NextCorner++];
		}
	}


}

void ASpawnVolume::SpawnObj()
{
	if (WhatToSpawn != NULL) {
		UWorld* const World = GetWorld();
		if (World) {
			
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;
			FVector SpawnLocation = getSpawnLocation();

			FRotator SpawnRotation;
			SpawnRotation.Yaw = std::rand();
			SpawnRotation.Pitch = std::rand();
			SpawnRotation.Roll = std::rand();
			AwallPart* const SpawnedWallPart = World->SpawnActor<AwallPart>((UClass*)WhatToSpawn, SpawnLocation, SpawnRotation, SpawnParams);
		}
	}
}

void ASpawnVolume::Tick(float DeltaSeconds)
{
	if (!spawned) {
		spawned = true;
		//PSmooth.BuildSpawnPoints();
		Walls = TArray<FVector>(PSmooth->Points);
		//Walls.Add(FVector(0.0f, 0.0f,  0.0f));

		FString s = FPaths::ConvertRelativePathToFull("");//"My vector " + FString::FromInt(NextCorner) + " is : " + FString::SanitizeFloat(Walls[NextCorner].X) + ", " + FString::SanitizeFloat(Walls[NextCorner].Y) + ", " + FString::SanitizeFloat(Walls[NextCorner].Z);
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, s);
		while(NextCorner < Walls.Num()) {
			
			SpawnObj();
		}
	}
}