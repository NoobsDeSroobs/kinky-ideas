// Fill out your copyright notice in the Description page of Project Settings.

#include "OculusTest.h"
#include "PointcloudSmoothener.h"
#include <stdlib.h>


PointcloudSmoothener::PointcloudSmoothener()
{ }

PointcloudSmoothener::PointcloudSmoothener(TCHAR* p)
{
	path = FString(p);
}

PointcloudSmoothener::PointcloudSmoothener(FString p)
{
	path = p;
	BuildSpawnPoints();
}

PointcloudSmoothener::~PointcloudSmoothener()
{
}

void PointcloudSmoothener::BuildSpawnPoints()
{
	TArray<FString> Lines;
	bool ReadSuccess = FFileHelper::LoadANSITextFileToStrings(*path, NULL, Lines);
	if (ReadSuccess) {
		Points = constructPointVectors(&Lines);
		Points = smoothByAvarage(Points);
		//Points = HalfNumPoints(Points);
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, "Ran the point calculations.");
	}
}

//General notes
/*
- When a potential corner is found we can improve the accuracy by checking the next point to see if we get a sharper corner.
If we do this several times, until the angle increases again we can find the sharpest angle and thus the most accurate corner.

*/




//Make vectors avereging the n next vectors and if the angle between the new temp vector and the current vector is great enough, treat as a corner.
//This will not work in tight hallways as one o the corners could be lost. Further, the distance that these n points represent vary 
//since the LIDAR unit can be close or far away, thus spacing the points differently. This could be solved by checking until the temp variable is > than a set limit.
TArray<FVector> PointcloudSmoothener::smoothByAvarage(TArray<FVector> pointList)
{
	size_t distance = 4;
	size_t end = pointList.Num() - distance;
	FVector basePlaneDirection = (pointList[distance] - pointList[0]);
	basePlaneDirection.Normalize();
	FVector newPlaneDirection;
	TArray<FVector> smoothPointCloud;


	smoothPointCloud.Add(pointList[0]);
	for (size_t i = 1; i < end; i++) {
		int size = pointList.Num();
		if (size == 0) size = 1;
		newPlaneDirection = pointList[(i + distance) % size] - pointList[i];

		FVector NormalizedBaseVector = basePlaneDirection;
		NormalizedBaseVector.Normalize();
		FVector NormalizedNewvector = newPlaneDirection;
		NormalizedNewvector.Normalize();

		//If a sharp turn/angle
		if (FVector::DotProduct(NormalizedBaseVector, NormalizedNewvector) < 0.7f) {
			smoothPointCloud.Add(pointList[i]);
			//smoothPointCloud.Add(pointList[i]);
			basePlaneDirection = NormalizedNewvector;
		}
	}

	smoothPointCloud.Shrink();


	return smoothPointCloud;
}

TArray<FVector> PointcloudSmoothener::HalfNumPoints(TArray<FVector> points)
{
	int divisor = 5;
	TArray<FVector> pointList;
	pointList.Reserve((points.Num()/divisor)+1);
	for (size_t i = 1; i < points.Num(); i+=divisor) {
		pointList.Add(points[i]);
	}

	pointList.Shrink();

	return pointList;
}
//
////After we have reduced the pointcloud to its bare minimum, readjust by making all angles orthogonal.
//std::vector<FVector> restoreOrthogonality(std::vector<FVector> points)
//{
//	std::vector<FVector> orthoPoints;
//	return orthoPoints;
//}
//
////Keep updating the base vector as we go until a potensial corner has been hit, then branch and check if said corner exists. 
////If it does, save the corner and jump to the corner point and restart the algorithm.
//std::vector<FVector> smoothByJumping(std::vector<FVector> points)
//{
//	std::vector<FVector> smoothPoints;
//	return smoothPoints;
//}
//
////A variant of the smooth by average algorithm. This algorithm assumes that all vectors that start from the end of the base vector 
////and lead to the next point can be a corner and only checks if the angle between these vectors is great enough.
//std::vector<FVector> smoothByAveragingBaseVector(std::vector<FVector> points)
//{
//	std::vector<FVector> smoothPoints(2);
//	smoothPoints[0] = points[0];
//	smoothPoints[1] = points[1];
//
//	FVector baseDir = points[3] - points[2];
//	FVector newDir;
//	FVector potensialCorner;
//	int counter = 2;
//	for (int i = 3; i < points.size() - 1; i++) {
//		//fprintf(stderr, "%f, %f, %f.\n", points[i].x, points[i].y, points[i].z);
//		newDir = points[i + 1] - points[i];
//		potensialCorner = points[i];
//		FVector innerNext = newDir;
//		for (int k = 1; k <= 5; k++) {
//			if ((i + k + 1) < points.size())
//				innerNext += points[i + k + 1] - points[i + k];
//		}
//		//If the new vector sufficiently differate from the current 
//		//base direction after 5 points we can assume there is a new wall
//		if (FVector::DotProduct(baseDir.Normalize(), innerNext.Normalize()) < 0.7f)
//		{
//			baseDir = innerNext;
//			smoothPoints.push_back(potensialCorner);
//			fprintf(stderr, "\n\n%f, %f, %f.\n", smoothPoints[counter].X, smoothPoints[counter].Y, smoothPoints[counter].Z);
//			counter++;
//			break;
//		}
//
//		baseDir += newDir;
//	}
//	return smoothPoints;
//}
//
////Since the LIDAR can guarantee some form of limit to the inaccuracy of its readings we can use this limit and say that all points 
////far enough from the base plane is not part of said plane. As such we can assume that the first point in the list to breach this limit is on the new wall.
////If we can create planes that intersect we can calculate the corners to be where they intersect.
//std::vector<FVector> smoothByOffsetToPlane(std::vector<FVector> points)
//{
//	std::vector<FVector> smoothPoints;
//
//	return smoothPoints;
//}


TArray<FVector> PointcloudSmoothener::constructPointVectors(TArray<FString> *data)
{
	//fprintf(stderr, "%s", data.c_str());
	TArray<FVector> points;//Size = 2 * numLines + 2.
	TArray<std::string> floats;//Size = 2 * numLines + 2.

	size_t pos = 0;
	size_t prev = 0;
	std::string token;
	
	//Split into Floats
	std::string delimiter =  " ";
	for (int i = 0; i < data->Num(); i++) {
		std::string currLine = std::string(TCHAR_TO_UTF8(*(*data)[i]));
		while ((pos = currLine.find(delimiter, prev)) != std::string::npos) {
			token = currLine.substr(prev, pos - prev);
			//fprintf(stderr, "%s\n", token.c_str());
			floats.Add(token);
			prev = pos + delimiter.size();
		}
		//Save the last flaots
		if (pos > currLine.size()) pos = currLine.size();
		token = currLine.substr(prev, pos - prev);
		floats.Add(token);
		pos = 0;
		prev = 0;
	}
	//fprintf(stderr, "floats.size = %d", floats.Num());

	//Parse all floats and save them
	for (int i = 0; i < floats.Num(); i += 2) {
		points.Add(FVector((float)::atof(floats[i].c_str()) / 5.f, (float)::atof(floats[i + 1].c_str()) / 5.f, 100.0f));
		//fprintf(stderr, "%f %f \n", (float)::atof(floats[i].c_str()), (float)::atof(floats[i+1].c_str()));
	}

	points.Shrink();

	return points;
}