// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "OculusTest.h"
#include "OculusTestHUD.h"
#include "Engine/Canvas.h"
#include "TextureResource.h"
#include "CanvasItem.h"

AOculusTestHUD::AOculusTestHUD(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshiarTexObj(TEXT("/Game/Textures/Crosshair"));
	CrosshairTex = CrosshiarTexObj.Object;
}


void AOculusTestHUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	//float farClippingPlane = 0;
	//FVector characterLineOfSight;//Normalized
	//FVector oculusLineOfSight;//Normalized
	//float t = farClippingPlane / Dot(characterLineOfSight, oculusLineOfSight);
	////t = d / (r.n)
	//FVector CLOFIntersectionPoint = characterLineOfSight*t;
	//FVector cursorLocationRelativeToScreenCenter = CLOFIntersectionPoint-(oculusLineOfSight*farClippingPlane);
	////p = t*r



	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition( (Center.X - (CrosshairTex->GetSurfaceWidth() * 0.5)),
										   (Center.Y - (CrosshairTex->GetSurfaceHeight() * 0.5f)) );

	// draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );
}

