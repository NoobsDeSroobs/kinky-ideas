// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "OculusTest.h"
#include "OculusTestGameMode.h"
#include "OculusTestHUD.h"
#include "OculusTestCharacter.h"

AOculusTestGameMode::AOculusTestGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/MyCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AOculusTestHUD::StaticClass();
}
