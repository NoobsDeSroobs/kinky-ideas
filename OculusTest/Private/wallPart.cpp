// Fill out your copyright notice in the Description page of Project Settings.

#include "OculusTest.h"
#include "wallPart.h"



AwallPart::AwallPart(const class FObjectInitializer& PCIP) : Super(PCIP)
{
	//Create
	WallCollisionComponent = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("WallBoxComponent"));
	//Set as root
	RootComponent = WallCollisionComponent;
	//Create
	WallMeshComponent = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("WallMeshComponent"));
	//Set
	WallMeshComponent->AttachTo(RootComponent);
	//Edit settings
	WallMeshComponent->SetSimulatePhysics(true);


}
