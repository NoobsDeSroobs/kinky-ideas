#pragma once

#include <glm\glm.hpp>
#include <glm\gtx\transform.hpp>

class Camera
{
	glm::vec3 position;
	glm::vec3 viewDir;
	glm::vec3 upVector;
	glm::vec2 center;
	glm::vec3 strafeDir;
	glm::float32_t MOVEMENT_SPEED;
	glm::float32_t MOUSE_SPEED;
public:
	glm::float32_t SPEEDBOOSTER;
	Camera(void);
	glm::mat4 getWorld2ViewMatrix() const{
		return glm::lookAt(position, position + viewDir, upVector);
	};
	void update(const glm::vec2 &newMousePosition);
	void moveForward();
	void moveBackward();
	void strafeLeft();
	void strafeRight();
	void ascend();
	void descend();
	void setCenter(int width, int height);
	glm::vec3 getPosition();

};

