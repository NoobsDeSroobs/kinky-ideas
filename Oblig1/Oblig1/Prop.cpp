#include "Prop.h"


Prop::Prop(void)
{
}

void Prop::generateCubeTowerMatrices(GLuint numLevels, glm::vec3 offset, Camera* camera, glm::mat4* projectionMatrix)
{	
	GLuint matrixLoc = 1;

	glm::vec2 startCoord;
	matrices[0] = *projectionMatrix * 
		glm::rotate(glm::translate(camera->getWorld2ViewMatrix(), glm::vec3((float)(0 + 0 + offset.x), (float)0 + offset.y, (float)(0 + 0 + offset.z))),
		0.0f, glm::vec3(0.0f, 1.0f, 0.0f)); 

	for(uint32_t i = 1; i < numLevels; ++i)//Skip the one we just made.
	{
		startCoord.x = (float)i+1;
		startCoord.y = (float)i+1;

		for (uint32_t j = i*2+1; j  > 0; --j)
		{
			for (uint32_t k = i*2+1; k > 0; --k)
			{
				matrices[matrixLoc] = (*projectionMatrix) * 
					glm::translate(camera->getWorld2ViewMatrix(), 
						glm::vec3((float)(startCoord.x - (float)j - offset.x), -(float)i + offset.y, (float)(startCoord.y - (float)k - offset.z)));
				++matrixLoc;
				//if(matrixLoc <= 9) break;
			}
		}
	}
}

void Prop::generateHollowCubeTowerMatrices(GLuint numLevels, glm::vec3 offset, Camera* camera, glm::mat4* projectionMatrix)
{	
	GLuint matrixLoc = 1;

	glm::vec2 startCoord;
	matrices[0] = glm::rotate(glm::translate(glm::mat4(), offset),
		0.0f, glm::vec3(0.0f, 0.0f, 1.0f));

	for(uint32_t i = 1; i < numLevels; ++i)//Skip the one we just made.
	{
		startCoord.x = (float)i+1;
		startCoord.y = (float)i+1;

		for (uint32_t j = i*2+1; j  > 0; --j)
		{
			for (uint32_t k = i*2+1; k > 0; --k)
			{
				if (j==1 || j == i*2+1 || k==1 || k == i*2+1 || i == numLevels-1)
				{
					matrices[matrixLoc] =  
						glm::translate(glm::mat4(), glm::vec3((float)(startCoord.x - (float)j - offset.x), -(float)i + offset.y, (float)(startCoord.y - (float)k - offset.z)));
					++matrixLoc;
				}
			}
		}
	}
}

void Prop::setShape(shape newShape)
{
	Shape = newShape;
}

shape Prop::gShape()
{
	return Shape;
}