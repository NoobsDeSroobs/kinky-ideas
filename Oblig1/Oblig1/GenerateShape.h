#pragma once

#include "Shape.h"

class GenerateShape
{
public:
	static shape makeTorus(GLfloat radius, GLfloat girth, GLfloat degrees);
	static shape makeSierpinski(GLint recursions, bool threeD);
	static shape makeSierpinskiTrue3D(GLint recursions);
	static shape makeSphere(shape sphere, GLuint recursions, float radius);
	static shape makeTetrahedron(float radius);
	static shape makeCube();
	static shape makeCube(glm::vec3 center);
	static shape generateRoom(std::vector<glm::vec3>pointList);
	GenerateShape(void);
	~GenerateShape(void);
};

