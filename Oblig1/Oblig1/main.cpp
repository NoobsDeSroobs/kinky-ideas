#define GLEW_STATIC
#define _USE_MATH_DEFINES

#include "Shape.h"
#include "PlaneSmoothening.h"
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <fstream>
#include <streambuf>
#include <string>
#include <cerrno>
#include <cmath>
#include <memory>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "Vertex.h"
#include "GenerateShape.h"
#include "Camera.h"
#include "Prop.h"


	bool running = true;

GLuint cubicPolynomial(GLuint num)
{
	if(num == 1) return 1;

	return (GLuint)(cubicPolynomial(num-1) + pow((num*2-1), 2));
}

GLuint hollowCubicPolynomial(GLuint num)
{
	if(num == 1) return 1;
			//Floor			 +	The rest of the layers.
	return (GLuint)(pow((num*2-1), 2) + pow(((num-1)*2-1), 2));
}

std::string get_file_contents(const char *filename)
{
	std::ifstream in(filename, std::ios::in | std::ios::binary);
	if (in)
	{
		return(std::string((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>()));
	}
	throw(errno);
}


GLuint compileShaders(void)
{
	GLint status;

	//Vertex shader
	std::string src = get_file_contents("VertexShader.txt");
	const char *vsSource = src.c_str();
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vsSource, NULL);
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
	if(status != GL_TRUE){
		printf("Vertex shader didnt compile propely.\n%s\n", vsSource);
		char buffer[512];
		glGetShaderInfoLog(vertexShader, 512, NULL, buffer);
		printf("%s\n", buffer);
		sf::sleep(sf::seconds(10.f));    
	}

	//Fragment shader
	src = get_file_contents("FragmentShader.txt");
	const GLchar* fsSource = src.c_str();
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fsSource, NULL);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);
	if(status != GL_TRUE){
		printf("Fragment shader didnt compile propely.\n%s\n", fsSource);
		char buffer[512];
		glGetShaderInfoLog(fragmentShader, 512, NULL, buffer);
		printf("%s\n", buffer);
		sf::sleep(sf::seconds(10.f));    
		running = false;
	}

	//Compile the final shader
	GLuint fullShader = glCreateProgram();
	glAttachShader(fullShader, vertexShader);
	glAttachShader(fullShader, fragmentShader);

	glBindFragDataLocation(fullShader, 0, "outColor");
	glLinkProgram(fullShader);
	glUseProgram(fullShader);	
	return fullShader;
}
//-4759.87350639 1761.52732898 -4551.64475743 4211.5546302 1354.84113319 -963.057924697 1333.93665051 -982.564794268 1319.31997028 -1006.64545199 1299.61699995 -1029.14076585 1280.00516596 -1049.88905372 1264.44704867 -1073.99006565 1243.58954001 -1092.39993523 1162.36543381 -1058.71462669 1191.73133818 -1122.29471068 1182.25635762 -1151.70315397 1157.96154181 -1168.74831772 1138.7058487 -1188.20959963 1129.15011077 -1218.17118146 1109.89846929 -1238.0884259 1088.15255338 -1255.92081083 1066.0136862 -1276.06911386 1049.93915552 -1300.91346069 1032.09805643 -1323.99968445 1004.41302561 -1336.68937079 976.606093947 -1345.72586353 984.783913763 -1405.60119013 949.194133276 -1406.41070099 930.261104991 -1433.33207563 913.449857291 -1458.28321005 892.907979501 -1481.46900158 868.420327333 -1499.42278813 849.045051683 -1526.80434248 827.536971498 -1548.21277956 806.090061085 -1573.54466521 783.999426806 -1596.37312329 764.733776617 -1628.36827865 738.053500196 -1642.00398701 718.148991021 -1675.31477317 693.515650378 -1697.81979099 667.806995798 -1715.87743702 644.989371324 -1745.38800081 624.79195326 -1779.76798422 1251.91179922 -3925.71485171 1187.37423331 -3953.02399184 1135.29968156 -4025.46902647 1068.1419794 -4052.59839015 1021.82927981 -4161.12334417 961.224081482 -4226.57527026 888.951449765 -4250.53779773 819.688145141 -4279.20090025 767.28763956 -4407.71433718 692.648769217 -4427.90252208 633.364233407 -4551.64475743 349.723689328 -2934.23215529 293.110376094 -2879.36961329 246.012217781 -2903.09494139 194.387014059 -2893.22722082 143.687729116 -2861.14424645 92.0796788312 -2812.74321531 44.0249127666 -2783.1518207 -3.71155058734 -2721.99746958 -51.5720288011 -2701.25774193 -95.9033985208 -2663.02367632 -140.321447297 -2622.74896653 -182.672815539 -2572.02116874 -232.459862214 -2529.8424185 -274.07235744 -2502.53683147 -312.657946034 -2457.69226283 -351.504387826 -2428.94786437 -390.592251037 -2402.45576305 -428.393089838 -2376.44628028 -461.825780192 -2328.39129256 -497.2061793 -2296.03170226 -534.171860496 -2274.10563155 -573.779119297 -2264.43668983 -634.30244183 -2180.61950195 -667.312047577 -2157.40299751 -703.76564945 -2138.15689571 -724.778805683 -2082.74416224 -759.294847675 -2061.62080274 -772.357859238 -1990.93535487 -796.75673225 -1955.13561476 -854.221915217 -1994.25052828 -867.882789318 -1933.66484014 -896.701018618 -1906.66627473 -947.323456962 -1928.93215741 -949.80721468 -1851.60126781 -975.581602002 -1823.98815452 -1024.06056596 -1762.60665131 -1057.12384941 -1748.53073451 -1110.7491912 -1701.24786458 -1153.03877898 -1700.43283436 -1197.21723335 -1700.90296495 -1222.60535904 -1673.15755341 -1255.24571603 -1659.16799703 -1332.46754153 -1581.83068413 -1295.76524478 -1484.87305616 -1332.8818178 -1474.64440536 -1386.30900924 -1482.576269 -1388.97101373 -1432.05604744 -1417.61910774 -1412.21707445 -1450.97432325 -1398.89733836 -1484.67117184 -1382.9651158 -1519.54942732 -1364.46128579 -1539.54200917 -1334.62228814 -1603.69784834 -1344.91717635 -1589.30519519 -1284.12599032 -1625.96450698 -1268.91515557 -1674.34548296 -1256.7057944 -1667.46566283 -1206.62808097 -1705.49541854 -1189.35526225 -1729.30170509 -1163.0011233 -1750.06408821 -1132.43805004 -1768.62422261 -1101.80870467 -1841.97218998 -1104.03564315 -1881.02859491 -1083.96145001 -1918.83933227 -1062.26229315 -1948.727055 -1035.47497682 -1978.63552986 -1006.12698128 -2011.1368366 -979.539496133 -2041.75055898 -950.728519291 -2068.97479073 -920.490937133 -2106.80548334 -894.28587033 -2142.71749127 -866.393856791 -2161.47208245 -829.711395185 -2193.47750007 -798.360519558 -2233.81415481 -769.845363873 -2264.74031721 -736.541611597 -2300.77099984 -705.475057174 -2340.30581123 -672.453500181 -2373.17400866 -637.277558932 -2408.99456644 -602.723602808 -2448.63772286 -569.534285345 -2387.45299286 -510.87225106 -2531.65376577 -497.120921168 -2568.37255967 -457.207783428 -2630.40140551 -421.763495233 -2663.84408634 -379.565865385 -2715.36850648 -339.420276394 -2750.79454136 -296.706848149 -2805.35149621 -253.147279642 -2858.40598211 -208.497581388 -2897.09870703 -159.753972411 -2952.87724499 -111.986499176 -3033.84827621 -62.8878916201 -3064.97766166 -11.7018587906 -3135.20852261 42.7524241711 -3191.20721848 99.2423862592 -3233.2297539 156.186934456 -3325.61432642 217.972278942 -3392.38571497 280.954019139 -3459.25679583 348.32884321 -3567.8282043 420.307214564 -3653.48036404 494.172836268 -3700.14885449 566.408878046 -3776.12428766 644.616883594 -3855.7401323 723.323601266 -3953.43286482 812.197379555 -4083.8278991 911.206756448 -4174.2287392 1008.16541069 -4271.46967546 1109.64966277 -4376.0558716 1217.44407311 -4537.83929081 1346.86184176 -4649.28506133 1468.70231189 -4759.87350639 1592.63139666 -4723.10519112 1672.53926819 -4633.48315216 1732.38836325 -4539.73991858 1788.25111049 -4542.53564618 1880.12870986 -4515.45120708 1961.91002826 -4361.16252268 1984.62432233 -4204.65824691 2002.70481828 -4106.74073742 2046.1472523 -4121.69682262 2141.335533 -4055.04266178 2760.98027404 -3912.67101086 2763.60283386 -2971.27956412 3423.71922247 -2871.84476368 3424.4274289 -2887.13588058 3567.30458043 -2788.12675959 3570.64619291 -2762.87900363 3664.38469239 -2699.68188449 3713.6631895 -2718.19538893 3875.23368038 -2651.21872491 3921.3596523 -2621.11847735 4024.14514869 -2536.94583612 4047.67029594 -2482.96227821 4111.99593567 -2445.34406513 4211.5546302 -2241.45193982 4015.23292618 -2054.67338866 3838.98558586 -1935.21634256 3770.07463719 -1797.45600014 3654.92557073 -1647.00187093 3502.04009645 -1530.83140839 3408.24284076 -1405.3696069 3278.47910898 -1299.36064674 3185.97276702 -1203.89388388 3105.83330466 -1031.32287184 2961.03586335 -942.00173336 2861.95767515 -857.778380711 2770.49928931 -804.244915689 2762.04284509 -727.59813814 2674.54650584 -552.028586979 2545.83766944 -496.30644632 2491.54975093 -444.638872794 2451.24923973 -393.162427869 2405.8363136 -343.157125622 2357.6576166 -290.798771426 2326.39556493 -245.047633501 2289.42305119 -200.824149519 2246.03977279 -158.280459012 2203.07146021 -117.627373532 2176.32351708 -79.6472911302 2146.52284148 -41.069598899 2120.85238773 -4.55530573409 2087.99503093 31.3399863947 2052.010689 66.6513673491 2019.15023159 100.063537686 1992.48896821 134.579200507 1970.91062425 164.613587886 1936.26520115 197.151802662 1910.85639615 226.928204544 1882.11892623 257.404635874 1857.24738617 286.262884539 1829.99552552 313.972249916 1803.62606678 341.641293948 1775.68276693 369.507576221 1759.62159316 397.770572065 1738.06431757 423.146889459 1715.07112387 448.770202695 1693.29069202 475.069820091 1670.51166669 491.443109989 1640.99136794 516.744181219 1621.92185807 540.659543857 1605.66818494 561.81414393 1575.58161648 586.144079741 1557.34746212 542.215486201 1362.31106177 338.00849639 789.703271089 350.95201486 779.652283564 362.167259674 769.535656107 372.435542558 760.445480715 383.965416661 751.041149877 395.955393384 742.240452246 407.138426841 730.732886484 416.893400237 721.171888553 430.258014692 713.862935579 439.494775913 702.912044245 450.438440437 692.373606788 462.638944443 683.074086088 473.587056922 674.002494073 482.424745819 664.000698133 494.569940945 655.944222868 503.56705689 645.261551012 515.313340572 638.492295199 525.971130453 626.480941394 536.289771975 619.655977519 550.011300278 607.509522202 559.007604979 596.846345449 571.038340534 587.788621564 581.306721717 579.407505808 593.057522176 569.902645537 601.844277395 560.615479421 613.767231806 552.03263958 624.399683931 541.886147827 635.299641554 531.016351387 645.082922577 522.66919318 658.045722659 514.410720524 668.944276921 502.657056923 679.372021026 493.592665613 692.301872119 483.630404194 701.9624871 474.873579707 714.834319218 465.88352468 728.349150792 455.399291326 738.72893615 444.696100045 749.052199911 434.101214359 760.755066971 424.137628698 774.557280534 412.923820664 785.8757522 401.504112189 800.078420765 392.116718116 806.67846044 343.453670764 819.641562929 332.457453398 833.014986386 318.98280903 845.772463469 309.14266939 863.161910085 299.054789257 875.292249558 284.927583543 887.244634163 272.846150145 904.094493744 260.045590008 919.359496871 247.147962794 937.327590276 236.690834228 954.703575855 222.331160098 973.552127044 210.26710615 985.84928285 195.259293004 998.801308111 180.612698656 1016.56106895 165.842072762 1034.76219507 149.457183322 1051.27724542 133.739357542 1067.39983186 116.604723063 1085.81578163 100.967016267 1109.62230622 83.3720578916 1131.05308405 66.3921761292 1149.28776978 47.0391829226 1168.93588363 27.1009680821 1194.98222574 6.51768161086 1212.42421038 -13.5566988492 1236.98299888 -35.7674501815 1262.90756184 -58.2456242971 1335.93612891 -82.8064579474 1359.51678399 -107.369346476 1388.79002526 -134.107524913 1421.80132547 -161.993798902 1452.81867965 -190.46114185 1491.00212404 -222.4160878 1523.27954937 -253.628620202 1556.8844236 -286.356231896 1599.94315536 -321.878703264 1643.99527797 -360.233987845 1687.94142966 -401.346147403 1736.25876352 -444.483485175 1761.52732898 -482.828871603 1749.09394891 -512.920968951 1723.13382209 -536.593322419 1708.05920294 -566.333831979 1677.86476763 -589.01857701 1661.7036866 -616.125734268 1643.53690041 -642.751133407 1620.35978615 -664.970986904 1603.18024672 -684.641263015 1578.47371943 -707.428992596 1559.32378041 -731.789312482 1537.7489288 -753.647452046 1511.92239863 -775.043005592 1497.9410475 -800.141038022 1479.60463639 -823.325987972 1451.14052349 -842.042898907 1434.98397869 -865.424198528 
std::vector<glm::vec3> constructPointVectors(std::string data)
{
	//fprintf(stderr, "%s", data.c_str());
	std::vector<glm::vec3> points;//Size = 2 * numLines + 2.
	std::vector<std::string> lines;
	std::vector<std::string> floats;//Size = 2 * numLines + 2.
	
	size_t pos = 0;
	size_t prev = 0;
	std::string token;

	//Split into lines
	std::string delimiter = "\n";
	while ((pos = data.find(delimiter, prev)) != std::string::npos) {
		token = data.substr(prev, pos - prev);
		//fprintf(stderr, "%s \n", token.c_str());
		lines.push_back(token);
		prev = pos+delimiter.size();
	}
	//Save the final line
	token = data.substr(prev, pos - prev);
	lines.push_back(token);

	fprintf(stderr, "Lines.size = %d", lines.size());


	//Split into seperate floats
	delimiter = " ";
	for (int i = 0; i < lines.size(); i++)
	{
		pos = 0;
		prev = 0;
		std::string currLine = lines[i];
		while ((pos = currLine.find(delimiter, prev)) != std::string::npos) {
			token = currLine.substr(prev, pos - prev);
			//fprintf(stderr, "%s\n", token.c_str());
			floats.push_back(token);
			prev = pos+delimiter.size();
		}
		//Save the last flaots
		token = currLine.substr(prev, pos - prev);
		floats.push_back(token);
	}
	fprintf(stderr, "floats.size = %d", floats.size());
	
	//Parse all floats and save them
	for (int i = 0; i < floats.size(); i+=2)
	{
		points.push_back(glm::vec3((float)::atof(floats[i].c_str())/1000.0f, 0.0f, (float)::atof(floats[i+1].c_str())/1000.0f));
		//fprintf(stderr, "%f %f \n", (float)::atof(floats[i].c_str()), (float)::atof(floats[i+1].c_str()));
	}

	return points;
}

GLuint height = 600;
GLuint width = 600;

void test()
{

	
	printf("sin 90: %f\n\n", sin(M_PI/4));
	int testArray[4][3] = { 1, 1, 1, 
							1, 1, 1, 
							1, 1, 1, 
							1, 1, 1};


	int i;	

	for (i = 0; i < 12; i++)
	{
		testArray[0][i] *= 255;
	}

	for (i = 0; i < 12; i++)
	{
		printf("%p - %d\n", &testArray[0][i], testArray[0][i]);
	}
	std::cin >> i;
}
int main()
{
	//test();
	//return 0;
	sf::Window window(sf::VideoMode::getDesktopMode(), "OpenGL", sf::Style::Close);
	window.setMouseCursorVisible(false);
	width = window.getSize().x;
	height = window.getSize().y;
	window.setFramerateLimit(60);

	glewExperimental = GL_TRUE;
	glewInit();
	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_CULL_FACE);
	glEnable(GL_LIGHTING);
	
	
	
	GLuint numPuramidLevels = 0;
	GLuint numVertFloats = 11;


	Camera Camera;
	Camera.setCenter(width, height);
	shape myCube;
	myCube = GenerateShape::generateRoom(smoothByAvarage(constructPointVectors(get_file_contents("point_file.txt"))));
	//myCube = GenerateShape::generateRoom(constructPointVectors(get_file_contents("point_file.txt")));
	Prop testProp;
	testProp.Shape = myCube;
	testProp.matrices.resize(hollowCubicPolynomial(numPuramidLevels)+1);
	glm::mat4 projectionMatrix = glm::perspective(80.0f, (float)width/(float)height, 0.1f, 1000.0f);


	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	GLuint vbo;
	glGenBuffers(1, &vbo); //Maybe I should gen all the buffers at once?
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, testProp.Shape.vertexBufferSize(), &myCube.vertices[0], GL_STATIC_DRAW);
	
	GLuint ebo;
	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		myCube.indexBufferSize(), &myCube.indices[0], GL_DYNAMIC_DRAW);


	

	GLuint fullShader = compileShaders();

	GLint posAttrib = glGetAttribLocation(fullShader, "modelPosition");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE,
		numVertFloats*sizeof(float), 0);

	GLint colAttrib = glGetAttribLocation(fullShader, "vertexColor");
	glEnableVertexAttribArray(colAttrib);
	glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE,
		numVertFloats*sizeof(float), (void*)(3*sizeof(float)));

		
	
	//Lighting
	GLuint ambientLightUniformLocation = glGetUniformLocation(fullShader, "ambientLight");
	glm::vec3 ambientLight(0.05f, 0.05f, 0.05f);
	glUniform3fv(ambientLightUniformLocation, 1, &ambientLight[0]);

	GLuint diffuseLightUniformLocation = glGetUniformLocation(fullShader, "diffuseLightPos");
	glm::vec3 diffuseLightPos(0.0f, -5.0f, 0.0f);
	testProp.matrices[hollowCubicPolynomial(numPuramidLevels)] = glm::translate(glm::mat4(), diffuseLightPos);
	//glUniform3fv(diffuseLightUniformLocation, 1, &diffuseLightPos[0]);

	
	GLuint eyePosWorldSpaceUniformLocation = glGetUniformLocation(fullShader, "eyePosWorldSpace");
	glUniform3fv(eyePosWorldSpaceUniformLocation, 1, &Camera.getPosition()[0]);

	GLuint projectionAndWorldToViewMatrixLocationID = glGetUniformLocation(fullShader, "projAndViewMatrix");
	glm::mat4 worldMatrix = projectionMatrix*Camera.getWorld2ViewMatrix(); 
	glUniformMatrix4fv(projectionAndWorldToViewMatrixLocationID, 1, GL_FALSE, &worldMatrix[0][0]);

	GLint normalAtrib = glGetAttribLocation(fullShader, "modelSpaceNormal");
	glEnableVertexAttribArray(normalAtrib);
	glVertexAttribPointer(normalAtrib, 3, GL_FLOAT, GL_FALSE,
		numVertFloats*sizeof(float), (void*)(6*sizeof(float)));

	
	//MatrixBuffer for the cubes
	GLuint matricesBufferID;
	glGenBuffers(1, &matricesBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, matricesBufferID);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void *)(sizeof(float) * 0));
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void *)(sizeof(float) * 4));
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void *)(sizeof(float) * 8));
	glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void *)(sizeof(float) * 12));
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glEnableVertexAttribArray(5);
	glVertexAttribDivisor(2, 1);
	glVertexAttribDivisor(3, 1);
	glVertexAttribDivisor(4, 1);
	glVertexAttribDivisor(5, 1);

	//
	//sf::Clock clock;

	//double lastTime = clock.getElapsedTime().asMilliseconds();
	//int nbFrames = 0;

	//// Measure speed
	//		double currentTime = clock.getElapsedTime().asMilliseconds();
	//		nbFrames++;
	//		if ( currentTime - lastTime >= 1.0 ){ // If last prinf() was more than 1 sec ago
	//			// printf and reset timer
	//			printf("%f ms/frame\n", 1000.0/double(nbFrames));
	//			nbFrames = 0;
	//			lastTime += 1.0;
	//		}

	//testProp.generateHollowCubeTowerMatrices(numPuramidLevels, glm::vec3(0.0f, 0.0f, 0.0f), &Camera, &projectionMatrix);

	while(running){
		sf::Event windowEvent;
		while(window.pollEvent(windowEvent)){
			switch (windowEvent.type)
			{
			case sf::Event::LostFocus:
				printf("Are you leaving!? G-good. I do not want you here anyways. BA-KA!\n\n");
				break;
			case sf::Event::KeyPressed:
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
				{
					Camera.SPEEDBOOSTER = 5.0f;
				}

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Num1))
				{
					glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
					glFrontFace(GL_CCW);
					glEnable(GL_CULL_FACE);
				}

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Num2))
				{
					glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
				}

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Num3))
				{
					glFrontFace(GL_CW);
				}

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Num4))
				{
					glDisable(GL_CULL_FACE);
				}

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Num5))
				{
					//Camera.SPEEDBOOSTER = 4.0f;
				}

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Num6))
				{
					//Camera.SPEEDBOOSTER = 4.0f;
				}

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Num7))
				{
					//Camera.SPEEDBOOSTER = 4.0f;
				}

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Num8))
				{
					//Camera.SPEEDBOOSTER = 4.0f;
				}

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Num9))
				{
					//Camera.SPEEDBOOSTER = 4.0f;
				}

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::T))
				{
					ambientLight.r = ambientLight.r+0.1f;
					ambientLight.g = ambientLight.g+0.1f;
					ambientLight.b = ambientLight.b+0.1f;
					glUniform3fv(ambientLightUniformLocation, 1, &ambientLight[0]);
				}

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::G))
				{
					ambientLight.r = ambientLight.r-0.1f;
					ambientLight.g = ambientLight.g-0.1f;
					ambientLight.b = ambientLight.b-0.1f;
					glUniform3fv(ambientLightUniformLocation, 1, &ambientLight[0]);
				}

				break;
			case sf::Event::KeyReleased:
				if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
				{
					Camera.SPEEDBOOSTER = 1.0f;
				}
				break;
			case sf::Event::GainedFocus:
				printf("Welcome back, master!!!! \nI knew you would not leave me~!<3<3<3<3<3<3<3<3<3<3\n\n");
				break;
			case sf::Event::Closed:
				running = false;
				break;

			default:
				break;
			}
		}

		if(sf::Event::KeyPressed){
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W))
			{
				Camera.moveForward();
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S))
			{
				Camera.moveBackward();
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A))
			{
				Camera.strafeLeft();
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D))
			{
				Camera.strafeRight();
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Space))
			{
				Camera.ascend();
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::F))
			{
				Camera.descend();
			}

			if (windowEvent.type == sf::Event::MouseMoved && sf::Mouse::isButtonPressed(sf::Mouse::Button::Right))
			{
				sf::Vector2i pos = sf::Mouse::getPosition(window);
				sf::Mouse::setPosition(sf::Vector2i(width/2, height/2), window);
				Camera.update(glm::vec2(pos.x, pos.y));
				
			} 
			

			glBindBuffer(GL_ARRAY_BUFFER, matricesBufferID);
			testProp.generateHollowCubeTowerMatrices(numPuramidLevels, glm::vec3(0.0f, 0.0f, 0.0f), &Camera, &projectionMatrix);			worldMatrix = projectionMatrix*Camera.getWorld2ViewMatrix(); 
			glUniformMatrix4fv(projectionAndWorldToViewMatrixLocationID, 1, GL_FALSE, &worldMatrix[0][0]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(glm::mat4)*testProp.matrices.size(), &testProp.matrices[0], GL_DYNAMIC_DRAW);

		}

		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Escape)){
			running = false;
			break;
		}
		glUniform3fv(diffuseLightUniformLocation, 1, &Camera.getPosition()[0]);

		// Clear the screen to black
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, width, height);

		//input uniforms
		glUniform3fv(eyePosWorldSpaceUniformLocation, 1, &Camera.getPosition()[0]);

		// Draw
		glDrawElementsInstanced(GL_TRIANGLES, (GLsizei)testProp.Shape.numIndices, GL_UNSIGNED_INT, 0, (GLsizei)testProp.matrices.size());

		// Swap buffers
		window.display();
	}

	glDeleteProgram(fullShader);
	//glDeleteShader(fragmentShader);
	//glDeleteShader(vertexShader);

	glDeleteBuffers(1, &ebo);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &matricesBufferID);

	//glDeleteVertexArrays(1, &vao);

	sf::sleep(sf::seconds(1.f));
	return 0;
}

/*
	How can one generate the indices automagically in a computer program? With only points?
	I read that you can go back into the pipeline to take a look at earlier stages or other polygons, is this true? If so, how?
	The OBJ file list faces as sets of location, normals and tex coords, but as I am using VBOs I see no way to convert this data to my format. 
	Is it possible or do I simply have to send the data down with no alteration and generate each triangle in the vertex shader?
*/