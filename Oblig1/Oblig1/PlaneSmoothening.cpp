#include "PlaneSmoothening.h"

//General notes
/*
- When a potential corner is found we can improve the accuracy by checking the next point to see if we get a sharper corner. 
If we do this several times, until the angle increases again we can find the sharpest angle and thus the most accurate corner.

*/




//Make vectors avereging the n next vectors and if the angle between the new temp vector and the current vector is great enough, treat as a corner.
//This will not work in tight hallways as one o the corners could be lost. Further, the distance that these n points represent vary 
//since the LIDAR unit can be close or far away, thus spacing the points differently. This could be solved by checking until the temp variable is > than a set limit.
std::vector<glm::vec3> smoothByAvarage(std::vector<glm::vec3> points){
	std::vector<glm::vec3> pointList(points.size()-2);

	for (int i = 2; i < points.size(); i++)
	{
		pointList[i-2] = points[i];
	}

	size_t distance = 4;
	size_t end = pointList.size()-distance;
	glm::vec3 basePlaneDirection = glm::normalize(pointList[distance] - pointList[0]);
	glm::vec3 newPlaneDirection;
	std::vector<glm::vec3> smoothPointCloud(2);
	
	smoothPointCloud[0] = points[0];
	smoothPointCloud[1] = points[1];
	
	 
	smoothPointCloud.push_back(pointList[0]);
	for (size_t i = 1; i < end; i++)
	{
		newPlaneDirection = pointList[(i+distance)%(pointList.size())] - pointList[i];
		if(glm::dot(glm::normalize(newPlaneDirection), basePlaneDirection) < 0.7f){
			smoothPointCloud.push_back(pointList[i]);
			smoothPointCloud.push_back(pointList[i]);
			basePlaneDirection = glm::normalize(newPlaneDirection);
		}
	}

	smoothPointCloud.shrink_to_fit();
	
	
	return smoothPointCloud;
}

//After we have reduced the pointcloud to its bare minimum, readjust by making all angles orthogonal.
std::vector<glm::vec3> restoreOrthogonality(std::vector<glm::vec3> points){
	std::vector<glm::vec3> orthoPoints;
	return orthoPoints;
}

//Keep updating the base vector as we go until a potensial corner has been hit, then branch and check if said corner exists. 
//If it does, save the corner and jump to the corner point and restart the algorithm.
std::vector<glm::vec3> smoothByJumping(std::vector<glm::vec3> points){
	std::vector<glm::vec3> smoothPoints;
	return smoothPoints;
}

//A variant of the smooth by average algorithm. This algorithm assumes that all vectors that start from the end of the base vector 
//and lead to the next point can be a corner and only checks if the angle between these vectors is great enough.
std::vector<glm::vec3> smoothByAveragingBaseVector(std::vector<glm::vec3> points){
	std::vector<glm::vec3> smoothPoints(2);
	smoothPoints[0] = points[0];
	smoothPoints[1] = points[1];

	glm::vec3 baseDir = points[3] - points[2];
	glm::vec3 newDir;
	glm::vec3 potensialCorner;
	int counter = 2;
	for (int i = 3; i < points.size()-1; i++)
	{
		//fprintf(stderr, "%f, %f, %f.\n", points[i].x, points[i].y, points[i].z);
		newDir = points[i+1] - points[i];
		potensialCorner = points[i];
		glm::vec3 innerNext = newDir;
		for (int k = 1; k  <= 5; k ++)
		{
			if((i+k+1) < points.size()) 
				innerNext += points[i+k+1] - points[i+k];
		}
		//If the new vector sufficiently differate from the current 
		//base direction after 5 points we can assume there is a new wall
		if(glm::dot(glm::normalize(baseDir), glm::normalize(innerNext)) < 0.7f){
			baseDir = innerNext;
			smoothPoints.push_back(potensialCorner);
			fprintf(stderr, "\n\n%f, %f, %f.\n", smoothPoints[counter].x, smoothPoints[counter].y, smoothPoints[counter].z);
			counter++;
			break;
		}
		
		baseDir += newDir;
	}
	return smoothPoints;
}

//Since the LIDAR can guarantee some form of limit to the inaccuracy of its readings we can use this limit and say that all points 
//far enough from the base plane is not part of said plane. As such we can assume that the first point in the list to breach this limit is on the new wall.
//If we can create planes that intersect we can calculate the corners to be where they intersect.
std::vector<glm::vec3> smoothByOffsetToPlane(std::vector<glm::vec3> points){
	std::vector<glm::vec3> smoothPoints;
	
	return smoothPoints;
}
