﻿#include "GenerateShape.h"
#include <glm/glm.hpp>
#include <glm\gtx\transform.hpp>
#include "Vertex.h"

#define NUM_ELEMENTS_ARRAY(a) sizeof(a)/ sizeof(*a)

shape GenerateShape::makeTorus(GLfloat radius, GLfloat girth, GLfloat degrees)
{
	GLuint vertsPerCircle = 360/degrees;
	//Axis
	glm::vec3 origoAxis(0.0f, 1.0f, 0.0f); // To spin around, 
	glm::vec3 centerAxis(0.0f, 0.0f, 1.0f);// To spin around the center of the torus
	glm::vec3 baseCenterAxis(0.0f, 0.0f, 1.0f);// To calculate the new center axis
	
	//Matrices
	glm::mat3x3 origoRotationMatrix = glm::mat3x3(glm::rotate(glm::mat4(), degrees, origoAxis));
	glm::mat3x3 centerRotationMatrix = glm::mat3x3(glm::rotate(glm::mat4(), degrees, centerAxis));

	//Vectors 
	glm::vec3 origoToCenter(radius, 0.0f, 0.0f); //The center of the torus tube to the surface of the tube
	glm::vec3 centerToSurface(girth, 0.0f, 0.0f); //The center of the shape to the center of the tube

	//Shape
	shape torus;


	GLuint i = 0;
	GLuint j = 0;
	while(1){
		//Set vert data
		vertex newVert;
		newVert.colour = glm::vec3(1.0f, 0.8431372549f, 0.0f);
		newVert.position = origoToCenter + centerToSurface;
		newVert.normal = glm::normalize(centerToSurface);
		//Add vert
		torus.vertices.push_back(newVert);
		i++;
		//Rotate the centerTo serface vector degrees degrees around the center axis
		centerToSurface = centerRotationMatrix*centerToSurface;
		//If we have made one circle
		if(i == vertsPerCircle)
		{
			i=0;
			j++;
			//Rotate origoToCenter and the center axis
			origoToCenter = origoRotationMatrix*origoToCenter;
			centerAxis = origoRotationMatrix*centerAxis;
			//Update the center rotation matrix so that it spins around the new axis
			centerRotationMatrix = glm::mat3x3(glm::rotate(glm::mat4(), degrees, centerAxis));
			centerToSurface = glm::normalize(origoToCenter)*girth;
		}
		if(j==vertsPerCircle) break;
	}

	for (i = 0; i  < vertsPerCircle*vertsPerCircle; i++)
	{
		//First triangle
		//x
		torus.indices.push_back(i);
		//y
		if(i+vertsPerCircle>=vertsPerCircle*vertsPerCircle)
			torus.indices.push_back(i+vertsPerCircle-(vertsPerCircle*vertsPerCircle));//To loop around the center of the model
		else
			torus.indices.push_back(i+vertsPerCircle);
		//z
		if((i+1)%vertsPerCircle == 0) //If we are not at the start and we have done one revolution
			torus.indices.push_back(i-vertsPerCircle+1);//We need to loop back to cover the last triangles. Around the center of the torus tube.
		else
			torus.indices.push_back(i+1);

		//Second triangle
		//x
		torus.indices.push_back(i);
		//y
		if((i+1)%vertsPerCircle == 0) //If we are not at the start and we have done one revolution
			torus.indices.push_back(i-vertsPerCircle+1);//We need to loop back to cover the last triangles. Around the center of the torus tube.
		else
			torus.indices.push_back(i+1);

		//z
		if((i+1)%vertsPerCircle == 0)
		{
			if(i>=vertsPerCircle*2-1)
				torus.indices.push_back(i-vertsPerCircle+1-vertsPerCircle);
			else
				torus.indices.push_back(i-vertsPerCircle+1+(vertsPerCircle*vertsPerCircle)-vertsPerCircle);

		}
		else if(i+1 <= vertsPerCircle)
			torus.indices.push_back(i-vertsPerCircle+1+(vertsPerCircle*vertsPerCircle));
		else
			torus.indices.push_back(i-vertsPerCircle+1);
	}
	//i, i+1, i+vertsPerCircle
	//i, i-vertsPerCircle+1, i+1

	torus.numIndices = (GLuint)torus.indices.size();
	torus.numVerts = (GLuint)torus.vertices.size();

	
	return torus;
}

shape GenerateShape::makeSierpinski(GLint recursions, bool threeD)
{
	shape retShape;
	retShape.indices.clear();
	retShape.vertices.clear();
	//The original triangle
	vertex temp[] = {
		glm::vec3(0.0f, 100.0f, -50.0f),	glm::vec3(1.0f, 0.8431372549f, 0.0f),	glm::vec3(0.0f, 0.0f, 1.0f),	glm::vec2(0, 0),	//2 - 2
		glm::vec3(-100.0f, -100.0f, -1.0f),	glm::vec3(1.0f, 0.8431372549f, 0.0f),	glm::vec3(0.0f, 0.0f, -1.0f),	glm::vec2(0, 0),	//0 - 0
		glm::vec3(100.0f,  -100.0f, -1.0f),	glm::vec3(1.0f, 0.8431372549f, 0.0f),	glm::vec3(0.0f,  0.0f, -1.0f),	glm::vec2(0, 0),	//1 - 1
	};
	retShape.vertices.push_back(temp[0]);
	retShape.vertices.push_back(temp[1]);
	retShape.vertices.push_back(temp[2]);

	retShape.indices.push_back(0);
	retShape.indices.push_back(1);
	retShape.indices.push_back(2);

	std::vector<GLuint> newIndices;

	for (int j = 0; j < recursions; j++)
	{
		for (GLuint i = 0; i < retShape.indices.size(); i+=3)
		{
			vertex oldVert1 = retShape.vertices[retShape.indices[i]];
			vertex oldVert2 = retShape.vertices[retShape.indices[i+1]];
			vertex oldVert3 = retShape.vertices[retShape.indices[i+2]];
			vertex newVert1;
			vertex newVert2;
			vertex newVert3;

			//Set the colour so it can be seen in the renderer
			newVert1.colour = glm::vec3(1.0f, 0.8431372549f, 0.0f);
			newVert2.colour = glm::vec3(1.0f, 0.8431372549f, 0.0f);
			newVert3.colour = glm::vec3(1.0f, 0.8431372549f, 0.0f);

			//Calculate the vector representing the edge between two vertices
			glm::vec3 v1Tov2 = oldVert2.position - oldVert1.position;
			glm::vec3 v2Tov3 = oldVert3.position - oldVert2.position;
			glm::vec3 v3Tov1 = oldVert1.position - oldVert3.position;

			//Halving it to point to the center point.
			v1Tov2 = v1Tov2 * 0.5f;
			v2Tov3 = v2Tov3 * 0.5f;
			v3Tov1 = v3Tov1 * 0.5f;

			//Set the position of each new vertex
			newVert1.position = oldVert1.position + v1Tov2;
			newVert2.position = oldVert2.position + v2Tov3;
			newVert3.position = oldVert3.position + v3Tov1;

			//Change it to be of length 1 so we can use it as normals
			newVert1.normal = glm::normalize(newVert1.position);
			newVert2.normal = glm::normalize(newVert2.position);
			newVert3.normal = glm::normalize(newVert3.position);

			//Add the new vertices and store the new indices
			retShape.vertices.push_back(newVert1);
			GLuint newVertIndex1 = (GLuint)retShape.vertices.size() - 1;
			retShape.vertices.push_back(newVert2);
			GLuint newVertIndex2 = newVertIndex1 + 1;
			retShape.vertices.push_back(newVert3);
			GLuint newVertIndex3 = newVertIndex2 + 1;

			//Calculate and add the new triangles; TRIFORCE!!!!
			//			/\
			//		   /__\
			//		  /\  /\
			//		 /__\/__\

			//Top
			newIndices.push_back(retShape.indices[i]);
			newIndices.push_back(newVertIndex1);
			newIndices.push_back(newVertIndex3);
			//Bottom left
			newIndices.push_back(retShape.indices[i+1]);
			newIndices.push_back(newVertIndex2);
			newIndices.push_back(newVertIndex1);
			//Bottom right
			newIndices.push_back(retShape.indices[i+2]);
			newIndices.push_back(newVertIndex3);
			newIndices.push_back(newVertIndex2);
		}
		retShape.numVerts = (GLuint)retShape.vertices.size();
		retShape.indices.swap(newIndices);
		newIndices.clear();
		retShape.numIndices = (GLuint)retShape.indices.size();
	}



	if(threeD)
	{
		GLfloat thickness = 3.0f;
		//Copy the current vertices one unit backwards
		int vertEnd = retShape.vertices.size();
		for (int i = 0; i < vertEnd; i++)
		{
			vertex behindVert = retShape.vertices[i];
			behindVert.position = behindVert.position + glm::vec3(0.0f, 0.0f, -thickness);
			retShape.vertices.push_back(behindVert);
		}

		int indEnd = (int)retShape.indices.size();
		for (int i = 0; i < indEnd; i+=3)
		{
			retShape.indices.push_back(retShape.indices[i] + vertEnd);
			retShape.indices.push_back(retShape.indices[i+2] + vertEnd);
			retShape.indices.push_back(retShape.indices[i+1] + vertEnd);			
		}

		//The walls
		for (int i = 0; i < indEnd; i+=3)
		{
			//Current indices
			

			//right
			int rightTopLeft = i;
			int rightTopRight = rightTopLeft + indEnd;
			int rightBottomLeft =  rightTopLeft + 2;
			int rightBottomRight = rightTopRight + 1;

			retShape.indices.push_back(retShape.indices[rightTopLeft]);
			retShape.indices.push_back(retShape.indices[rightBottomLeft]);
			retShape.indices.push_back(retShape.indices[rightTopRight]);

			retShape.indices.push_back(retShape.indices[rightTopRight]);
			retShape.indices.push_back(retShape.indices[rightBottomLeft]);
			retShape.indices.push_back(retShape.indices[rightBottomRight]);

			//left
			int leftTopLeft = i + indEnd;
			int leftTopRight = i;
			int leftBottomLeft =  leftTopLeft + 2;
			int leftBottomRight = leftTopRight + 1;

			retShape.indices.push_back(retShape.indices[leftTopLeft]);
			retShape.indices.push_back(retShape.indices[leftBottomLeft]);
			retShape.indices.push_back(retShape.indices[leftTopRight]);

			retShape.indices.push_back(retShape.indices[leftTopRight]);
			retShape.indices.push_back(retShape.indices[leftBottomLeft]);
			retShape.indices.push_back(retShape.indices[leftBottomRight]);

			//bottom
			
			retShape.indices.push_back(retShape.indices[leftBottomLeft]);
			retShape.indices.push_back(retShape.indices[rightBottomRight]);
			retShape.indices.push_back(retShape.indices[leftBottomRight]);

			retShape.indices.push_back(retShape.indices[leftBottomRight]);
			retShape.indices.push_back(retShape.indices[rightBottomRight]);
			retShape.indices.push_back(retShape.indices[rightBottomLeft]);

		}

		retShape.numVerts = (GLuint)retShape.vertices.size();
		retShape.numIndices = (GLuint)retShape.indices.size();
	}



return retShape;
}

shape GenerateShape::makeSierpinskiTrue3D(GLint recursions)
{
	shape retShape = makeTetrahedron(10);

	std::vector<GLuint> newIndices;
	
	
	GLuint i;
	
	for (int rec= 0; rec < recursions; rec++)
	{
		for (i = 0; i < retShape.numIndices; i+=12)
		{
			//0, 2, 1, 
			//0, 3, 2, 
			//0, 1, 3, 
			//1, 2, 3
			//The next tetrahedron
			vertex vTop		= retShape.vertices[retShape.indices[i]];
			vertex vTriTop	= retShape.vertices[retShape.indices[i+1]]; //Top
			vertex vTriRight = retShape.vertices[retShape.indices[i+2]]; //Right
			vertex vTriLeft	= retShape.vertices[retShape.indices[i+4]]; //Left
		
		
			//The new vertices to be made
			vertex vTopTriTop;
			vertex vTopTriRight;
			vertex vTopTriLeft;
		
			vertex vFloorTriLeft;
			vertex vFloorTriRight;
			vertex vFloorTriBot;

			//Set the colour so it can be seen in the renderer
			vTopTriTop.colour		= glm::vec3(1.0f, 0.8431372549f, 0.0f);
			vTopTriRight.colour		= glm::vec3(1.0f, 0.0f, 0.0f);
			vTopTriLeft.colour		= glm::vec3(1.0f, 0.8431372549f, 0.0f);
			vFloorTriLeft.colour	= glm::vec3(1.0f, 0.8431372549f, 0.0f);
			vFloorTriRight.colour	= glm::vec3(1.0f, 0.0f, 0.0f);
			vFloorTriBot.colour		= glm::vec3(1.0f, 0.0f, 0.0f);
		
			//Calculate the vector representing the edge between two vertices
			//From the peak down to each corner
			glm::vec3 topTopOffset	 =  vTriTop.position	-	vTop.position;
			glm::vec3 topRightOffset =  vTriRight.position	-	vTop.position;	
			glm::vec3 topLeftOffset  =  vTriLeft.position	-	vTop.position;	
			//From a corner to another corner
			glm::vec3 floorLeftOffset	= vTriTop.position  - vTriLeft.position;
			glm::vec3 floorRightOffset	= vTriTop.position  - vTriRight.position;
			glm::vec3 floorBotOffset	= vTriLeft.position - vTriRight.position;

			//Halving it to point to the center point.
			topTopOffset   = topTopOffset   * 0.5f;
			topRightOffset = topRightOffset * 0.5f;
			topLeftOffset  = topLeftOffset  * 0.5f;

			floorLeftOffset	 = floorLeftOffset	* 0.5f;
			floorRightOffset = floorRightOffset	* 0.5f;
			floorBotOffset	 = floorBotOffset	* 0.5f;

			//Set the position of each new vertex
			vTopTriTop.position		= vTop.position + topTopOffset;
			vTopTriRight.position	= vTop.position + topRightOffset;
			vTopTriLeft.position	= vTop.position + topLeftOffset;

			vFloorTriLeft.position	= vTriLeft.position  + floorLeftOffset;
			vFloorTriRight.position = vTriRight.position + floorRightOffset;
			vFloorTriBot.position	= vTriRight.position + floorBotOffset;

		
			//Add the new vertices and store the new indices
			retShape.vertices.push_back(vTopTriTop);
			GLuint topTriTopIndex   = (GLuint)retShape.vertices.size() - 1;
			retShape.vertices.push_back(vTopTriRight);
			GLuint topTriRightIndex = topTriTopIndex + 1;
			retShape.vertices.push_back(vTopTriLeft);
			GLuint topTriLeftIndex  = topTriRightIndex + 1;

			retShape.vertices.push_back(vFloorTriLeft);
			GLuint floorTriLeftIndex  = topTriLeftIndex + 1;
			retShape.vertices.push_back(vFloorTriRight);
			GLuint floorTriRightIndex = floorTriLeftIndex + 1;
			retShape.vertices.push_back(vFloorTriBot);
			GLuint floorTriBotIndex   = floorTriRightIndex + 1;
		
			//Tetrahedron 1 Top
			newIndices.push_back(retShape.indices[i]);
			newIndices.push_back(topTriTopIndex);
			newIndices.push_back(topTriRightIndex);
		
			newIndices.push_back(retShape.indices[i]);
			newIndices.push_back(topTriRightIndex);
			newIndices.push_back(topTriLeftIndex);
		
			newIndices.push_back(retShape.indices[i]);
			newIndices.push_back(topTriLeftIndex);
			newIndices.push_back(topTriTopIndex);
		
			newIndices.push_back(topTriTopIndex);
			newIndices.push_back(topTriLeftIndex);
			newIndices.push_back(topTriRightIndex);

			//Tetrahedron 2 TriTop
			newIndices.push_back(topTriTopIndex);
			newIndices.push_back(retShape.indices[i+1]);
			newIndices.push_back(floorTriRightIndex);
		
			newIndices.push_back(topTriTopIndex);
			newIndices.push_back(floorTriRightIndex);
			newIndices.push_back(floorTriLeftIndex);
		
			newIndices.push_back(topTriTopIndex);
			newIndices.push_back(floorTriLeftIndex);
			newIndices.push_back(retShape.indices[i+1]);
		
			newIndices.push_back(retShape.indices[i+1]);
			newIndices.push_back(floorTriLeftIndex);
			newIndices.push_back(floorTriRightIndex);
		
			//Tetrahedron 3 TriRight
			newIndices.push_back(topTriRightIndex);
			newIndices.push_back(floorTriRightIndex);
			newIndices.push_back(retShape.indices[i+4]);
		
			newIndices.push_back(topTriRightIndex);
			newIndices.push_back(retShape.indices[i+4]);
			newIndices.push_back(floorTriBotIndex);
		
			newIndices.push_back(topTriRightIndex);
			newIndices.push_back(floorTriBotIndex);
			newIndices.push_back(floorTriRightIndex);
		
			newIndices.push_back(floorTriRightIndex);
			newIndices.push_back(floorTriBotIndex);
			newIndices.push_back(retShape.indices[i+4]);
		
			//Tetrahedron 4 TriLeft
			newIndices.push_back(topTriLeftIndex);
			newIndices.push_back(floorTriLeftIndex);
			newIndices.push_back(floorTriBotIndex);
		
			newIndices.push_back(topTriLeftIndex);
			newIndices.push_back(floorTriBotIndex);
			newIndices.push_back(retShape.indices[i+3]);
		
			newIndices.push_back(topTriLeftIndex);
			newIndices.push_back(retShape.indices[i+3]);
			newIndices.push_back(floorTriBotIndex);
		
			newIndices.push_back(floorTriLeftIndex);
			newIndices.push_back(retShape.indices[i+3]);
			newIndices.push_back(floorTriBotIndex);


		}
		retShape.indices.swap(newIndices);
		newIndices.clear();
		
		retShape.numIndices = (GLuint) retShape.indices.size();
		retShape.numVerts = (GLuint) retShape.vertices.size();

	}

	for (int i = 0; i < retShape.vertices.size(); i++)
	{
		printf("Vert %d, X:%f, Y:%f, Z:%f\n", i, retShape.vertices[i].position.x, retShape.vertices[i].position.y, retShape.vertices[i].position.z);
	}
	return retShape;
}

shape GenerateShape::makeSphere(shape sphere, GLuint recursions, float radius)
{

	if(sphere.numVerts == 0)//AKA the shape is empty
		sphere = makeTetrahedron(radius);// Make the base; a tetrahedron.

	if(!recursions) //If there are no recursions to make left.
		return sphere;

	std::vector<GLuint> newIndices;

	for (GLuint i = 0; i < sphere.numIndices; i+=3)
	{
		vertex oldVert1 = sphere.vertices[sphere.indices[i]];
		vertex oldVert2 = sphere.vertices[sphere.indices[i+1]];
		vertex oldVert3 = sphere.vertices[sphere.indices[i+2]];
		vertex newVert1;
		vertex newVert2;
		vertex newVert3;
		
		//Set the colour so it can be seen in the renderer
		newVert1.colour = glm::vec3(0.5f, 0.2f, 0.9f);
		newVert2.colour = glm::vec3(0.8f, 0.7f, 0.6f);
		newVert3.colour = glm::vec3(0.45f, 1.0f, 0.0f);

		//Calculate the vector representing the edge between two vertices
		glm::vec3 v1Tov2 = oldVert2.position - oldVert1.position;
		glm::vec3 v2Tov3 = oldVert3.position - oldVert2.position;
		glm::vec3 v3Tov1 = oldVert1.position - oldVert3.position;

		//Halving it to point to the center point.
		v1Tov2 = v1Tov2 * 0.5f;
		v2Tov3 = v2Tov3 * 0.5f;
		v3Tov1 = v3Tov1 * 0.5f;

		//Set the position of each new vertex
		newVert1.position = oldVert1.position + v1Tov2;
		newVert2.position = oldVert2.position + v2Tov3;
		newVert3.position = oldVert3.position + v3Tov1;

		//Change it to be of length 1 so we can use it as normals
		newVert1.normal = glm::normalize(newVert1.position);
		newVert2.normal = glm::normalize(newVert2.position);
		newVert3.normal = glm::normalize(newVert3.position);

		//Set the final position on the membrane at length radius
		newVert1.position = newVert1.normal * radius;
		newVert2.position = newVert2.normal * radius;
		newVert3.position = newVert3.normal * radius;

		//Add the new vertices and store the new indices
		sphere.vertices.push_back(newVert1);
		GLuint newVertIndex1 = (GLuint)sphere.vertices.size() - 1;
		sphere.vertices.push_back(newVert2);
		GLuint newVertIndex2 = newVertIndex1 + 1;
		sphere.vertices.push_back(newVert3);
		GLuint newVertIndex3 = newVertIndex2 + 1;

		//Calculate and add the new triangles; TRIFORCE!!!!
//			/\
//		   /__\
//		  /\  /\
//		 /__\/__\

		//Top
		newIndices.push_back(sphere.indices[i]);
		newIndices.push_back(newVertIndex1);
		newIndices.push_back(newVertIndex3);
		//Bottom left
		newIndices.push_back(sphere.indices[i+1]);
		newIndices.push_back(newVertIndex2);
		newIndices.push_back(newVertIndex1);
		//Bottom right
		newIndices.push_back(sphere.indices[i+2]);
		newIndices.push_back(newVertIndex3);
		newIndices.push_back(newVertIndex2);
		//Center
		newIndices.push_back(newVertIndex1);
		newIndices.push_back(newVertIndex2);
		newIndices.push_back(newVertIndex3);


	}

	//Set important data
	sphere.indices.resize(newIndices.size());
	sphere.indices.swap(newIndices);

	sphere.numVerts = (GLuint)sphere.vertices.size();
	sphere.numIndices = (GLuint)sphere.indices.size();

	return makeSphere(sphere, recursions-1, radius);
}

shape GenerateShape::makeTetrahedron(float radius)
{
	shape retShape;
	vertex verts[] = {
		//Position															//Colour						//Normal vector					//Corner ID-Vertex
		glm::normalize(glm::vec3(-1.0f, 0.0f, -1.0f/sqrtf(2.0f)))*radius,	glm::vec3(1.0f, 0.0f, 0.0f),	glm::normalize(glm::vec3(-1.0f, 0.0f, -1.0f/sqrtf(2.0f))),	glm::vec2(0.0f, 0.0f),//0 - 0
		glm::normalize(glm::vec3(1.0f,  0.0f, -1.0f/sqrtf(2.0f)))*radius,	glm::vec3(1.0f, 1.0f, 0.0f),	glm::normalize(glm::vec3(1.0f,  0.0f, -1.0f/sqrtf(2.0f))),	glm::vec2(0.0f, 0.0f),//1 - 1
		glm::normalize(glm::vec3(0.0f, -1.0f, 1.0f/sqrtf(2.0f)))*radius,	glm::vec3(1.0f, 0.0f, 1.0f),	glm::normalize(glm::vec3(0.0f, -1.0f, 1.0f/sqrtf(2.0f))),	glm::vec2(0.0f, 0.0f),//2 - 2
		glm::normalize(glm::vec3(0.0f, 1.0f, 1.0f/sqrtf(2.0f)))*radius,		glm::vec3(0.0f, 1.0f, 1.0f),	glm::normalize(glm::vec3(0.0f, 1.0f, 1.0f/sqrtf(2.0f))),	glm::vec2(0.0f, 0.0f),	//3 - 3
	};

	//Triangle sets
	GLuint elements[] = {
		//Front
		0, 1, 2, 
		2, 1, 3, 

		//Back																											    
		0, 2, 3, 
		0, 3, 1, 
	};

	retShape.numVerts = NUM_ELEMENTS_ARRAY(verts);
	retShape.vertices.resize(retShape.numVerts);
	retShape.numIndices = NUM_ELEMENTS_ARRAY(elements);
	retShape.indices.resize(retShape.numIndices);

	for (uint16_t i = 0; i < retShape.numVerts; i++)
	{
		retShape.vertices[i] = verts[i];
	}

	for (uint16_t i = 0; i < retShape.numIndices; i++)
	{
		retShape.indices[i] = elements[i];
	}

	return retShape;
}

shape GenerateShape::makeCube()
{
	shape retShape;
	vertex verts[] = {
		//Position						//Colour						//Normal vector									//Corner ID-Vertex
		//Front
		glm::vec3(-0.5f, -0.5f, 0.5f),	glm::vec3(1.0f, 0.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 1.0f),	glm::vec2(0,0),	//0 - 0
		glm::vec3(0.5f, -0.5f, 0.5f),	glm::vec3(1.0f, 0.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 1.0f),	glm::vec2(0,0),	//1 - 1
		glm::vec3(0.5f, 0.5f, 0.5f),	glm::vec3(1.0f, 0.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 1.0f),	glm::vec2(0,0),	//2 - 2
		glm::vec3(-0.5f, 0.5f, 0.5f),	glm::vec3(1.0f, 0.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 1.0f),	glm::vec2(0,0),	//3 - 3
		//Back																			   
		glm::vec3(-0.5f, 0.5f, -0.5f),	glm::vec3(1.0f, 1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, -1.0f),	glm::vec2(0,0),	//4 - 4
		glm::vec3(-0.5f, -0.5f, -0.5f),	glm::vec3(1.0f, 1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, -1.0f),	glm::vec2(0,0),	//5 - 5
		glm::vec3(0.5f, -0.5f, -0.5f),	glm::vec3(1.0f, 1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, -1.0f),	glm::vec2(0,0),	//6 - 6
		glm::vec3(0.5f, 0.5f, -0.5f),	glm::vec3(1.0f, 1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, -1.0f),	glm::vec2(0,0),	//7 - 7
		//Top																	  
		glm::vec3(-0.5f, -0.5f, 0.5f),	glm::vec3(0.0f, 1.0f, 1.0f),	glm::vec3(0.0f, -1.0f, 0.0f),	glm::vec2(0,0),	//0 - 8
		glm::vec3(0.5f, -0.5f, 0.5f),	glm::vec3(0.0f, 1.0f, 1.0f),	glm::vec3(0.0f, -1.0f, 0.0f),	glm::vec2(0,0),	//1 - 9
		glm::vec3(-0.5f, -0.5f, -0.5f),	glm::vec3(0.0f, 1.0f, 1.0f),	glm::vec3(0.0f, -1.0f, 0.0f),	glm::vec2(0,0),	//5 - 10
		glm::vec3(0.5f, -0.5f, -0.5f),	glm::vec3(0.0f, 1.0f, 1.0f),	glm::vec3(0.0f, -1.0f, 0.0f),	glm::vec2(0,0),	//6 - 11
		//Bottom																					
		glm::vec3(0.5f, 0.5f, 0.5f),	glm::vec3(0.0f, 0.0f, 1.0f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec2(0,0),	//2 - 12
		glm::vec3(-0.5f, 0.5f, 0.5f),	glm::vec3(0.0f, 0.0f, 1.0f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec2(0,0),	//3 - 13
		glm::vec3(-0.5f, 0.5f, -0.5f),	glm::vec3(0.0f, 0.0f, 1.0f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec2(0,0),	//4 - 14
		glm::vec3(0.5f, 0.5f, -0.5f),	glm::vec3(0.0f, 0.0f, 1.0f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec2(0,0),	//7 - 15
		//Left													
		glm::vec3(-0.5f, -0.5f, 0.5f),	glm::vec3(1.0f, 0.0f, 1.0f),	glm::vec3(-1.0f, 0.0f, 0.0f),	glm::vec2(0,0),	//0 - 16
		glm::vec3(-0.5f, 0.5f, 0.5f),	glm::vec3(1.0f, 0.0f, 1.0f),	glm::vec3(-1.0f, 0.0f, 0.0f),	glm::vec2(0,0),	//3 - 17
		glm::vec3(-0.5f, 0.5f, -0.5f),	glm::vec3(1.0f, 0.0f, 1.0f),	glm::vec3(-1.0f, 0.0f, 0.0f),	glm::vec2(0,0),	//4 - 18
		glm::vec3(-0.5f, -0.5f, -0.5f),	glm::vec3(1.0f, 0.0f, 1.0f),	glm::vec3(-1.0f, 0.0f, 0.0f),	glm::vec2(0,0),	//5 - 19
		//Right													
		glm::vec3(0.5f, -0.5f, 0.5f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec3(1.0f, 0.0f, 0.0f),	glm::vec2(0,0),	//1 - 20
		glm::vec3(0.5f, 0.5f, 0.5f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec3(1.0f, 0.0f, 0.0f),	glm::vec2(0,0),	//2 - 21
		glm::vec3(0.5f, -0.5f, -0.5f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec3(1.0f, 0.0f, 0.0f),	glm::vec2(0,0),	//6 - 22
		glm::vec3(0.5f, 0.5f, -0.5f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec3(1.0f, 0.0f, 0.0f),	glm::vec2(0,0),	//7 - 23

	};

	//Triangle sets
	GLuint elements[] = {
		//Front
		0, 1, 3, 
		1, 2, 3, 

		//Back																											    
		5, 4, 6, 
		4, 7, 6, 

		//Top	
		11, 8, 10, 
		11, 9, 8,

		//Bottom			
		13, 15, 14, 
		13, 12, 15, 

		//Left	
		19, 17, 18, 
		19, 16, 17, 

		//Right	
		20, 23, 21, 
		20, 22, 23
	};

	retShape.numVerts = NUM_ELEMENTS_ARRAY(verts);
	retShape.vertices.resize(retShape.numVerts);
	retShape.numIndices = NUM_ELEMENTS_ARRAY(elements);
	retShape.indices.resize(retShape.numIndices);

	for (uint16_t i = 0; i < retShape.numVerts; i++)
	{
		retShape.vertices[i] = verts[i];
	}

	for (uint16_t i = 0; i < retShape.numIndices; i++)
	{
		retShape.indices[i] = elements[i];
	}

	return retShape;
}

shape GenerateShape::makeCube(glm::vec3 center)
{
	//The center is in world coordinates

	shape retShape;
	vertex verts[] = {
		//Position					  //Colour						//Vertex ID
		glm::vec3(0.0f + center.x, 0.0f + center.y, 0.5f + center.z),  glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(0, 0),	//0
		glm::vec3(0.5f + center.x, 0.0f + center.y, 0.0f + center.z),  glm::vec3(1.0f, 0.0f, 0.3f), glm::vec3(1.0f, 0.0f, 0.3f), glm::vec2(0, 0),	//1
		glm::vec3(0.5f + center.x, 0.5f + center.y, 0.0f + center.z),  glm::vec3(1.0f, 0.5f, 0.0f), glm::vec3(1.0f, 0.5f, 0.0f), glm::vec2(0, 0),	//2
		glm::vec3(0.0f + center.x, 0.5f + center.y, 0.5f + center.z),  glm::vec3(1.0f, 0.5f, 0.3f), glm::vec3(1.0f, 0.5f, 0.3f), glm::vec2(0, 0),	//3
																								    						   	 
		glm::vec3(-0.5f + center.x, 0.5f + center.y, 0.0f + center.z), glm::vec3(0.0f, 0.5f, 0.3f), glm::vec3(0.0f, 0.5f, 0.3f), glm::vec2(0, 0),	//4
		glm::vec3(-0.5f + center.x, 0.0f + center.y, 0.0f + center.z), glm::vec3(1.0f, 0.9f, 0.3f), glm::vec3(1.0f, 0.9f, 0.3f), glm::vec2(0, 0),	//5
		glm::vec3(0.0f + center.x, 0.0f + center.y, -0.5f + center.z), glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f), glm::vec2(0, 0),	//6
		glm::vec3(0.0f + center.x, 0.5f + center.y, -0.5f + center.z), glm::vec3(1.0f, 1.0f, 0.3f), glm::vec3(1.0f, 1.0f, 0.3f),  glm::vec2(0, 0),	//7

	};

	//Triangle sets
	GLuint elements[] = {
		0, 1, 2,
		0, 2, 3,
		0, 4, 5,
		0, 3, 4,
		0, 5, 6,
		0, 6, 1,

		1, 2, 7,
		2, 3, 7,
		3, 4, 7,
		4, 5, 7,
		5, 6, 7,
		6, 1, 7
	};

	retShape.numVerts = NUM_ELEMENTS_ARRAY(verts);
	retShape.vertices.resize(retShape.numVerts);
	retShape.numIndices = NUM_ELEMENTS_ARRAY(elements);
	retShape.indices.resize(retShape.numIndices);

	for (uint16_t i = 0; i < retShape.numVerts; i++)
	{
		retShape.vertices[i] = verts[i];
	}

	for (uint16_t i = 0; i < retShape.numIndices; i++)
	{
		retShape.indices[i] = elements[i];
	}

	return retShape;
}

shape GenerateShape::generateRoom(std::vector<glm::vec3> pointList){
	pointList.shrink_to_fit();
	std::vector<glm::vec3> points(pointList.size()-2);
	for (int i = 2; i < pointList.size(); i ++)
	{
		points[i-2] = pointList[i];
	}

	GLuint arraySize = (GLuint)points.size();
	shape room;
	room.center = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 height(0.0f, 3.0f, 0.0f);
	
	//We have two vertices for every point given
	room.vertices.reserve(arraySize*2);
	//We need three indices for each edge
	room.vertices.reserve(arraySize*3);


	glm::vec3 colour(1.0f, 1.0f, 1.0f);
	glm::vec3 normal;
	glm::vec2 UV;
	for (int i = 0; i < arraySize; i++)
	{ 
		vertex vBottom;
		vBottom.position = points[i];
		//fprintf(stderr, "%f %f %f\n", points[i].x, points[i].y, points[i].z);
		vBottom.colour = colour;
		if(i%2 == 0)
			vBottom.normal = glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), glm::normalize(points[i+1]-points[i]));
		else
			vBottom.normal = room.vertices[i-1].normal;
		vBottom.UV = UV;
		room.vertices.push_back(vBottom);
	}

	for (int i = 0; i < arraySize; i++)
	{
		vertex vTop;
		vTop.position = points[i]+height;
		vTop.colour = colour;
		vTop.normal = room.vertices[i].normal;
		vTop.UV = UV;
		room.vertices.push_back(vTop);
	}

	for (GLuint i = 0; i < arraySize; i++)
	{
		//Bottom half
		room.indices.push_back(i);
		room.indices.push_back(((i+1)%arraySize)+arraySize);
		room.indices.push_back((i+1)%arraySize);
		//Top half
		room.indices.push_back(i);
		room.indices.push_back(i+arraySize);
		room.indices.push_back(((i+1)%arraySize)+arraySize);
	}
	

	//Ceiling and floor surface
	size_t wallEnd = arraySize*2;

	float minX = pointList[0].x;
	float maxX = pointList[0].z;
	float minY = pointList[1].x;
	float maxY = pointList[1].z;

	vertex vFloor1;
	vertex vFloor2;
	vertex vFloor3;
	vertex vFloor4;

	vFloor1.position = glm::vec3(minX, 0.0f, maxY);
	vFloor2.position = glm::vec3(maxX, 0.0f, maxY);
	vFloor3.position = glm::vec3(maxX, 0.0f, minY);
	vFloor4.position = glm::vec3(minX, 0.0f, minY);
	
	vFloor1.colour = colour;
	vFloor2.colour = colour;
	vFloor3.colour = colour;
	vFloor4.colour = colour;

	vFloor1.normal = glm::vec3(0.0f, 1.0f, 0.0f);
	vFloor2.normal = glm::vec3(0.0f, 1.0f, 0.0f);
	vFloor3.normal = glm::vec3(0.0f, 1.0f, 0.0f);
	vFloor4.normal = glm::vec3(0.0f, 1.0f, 0.0f);

	room.vertices.push_back(vFloor1);
	room.vertices.push_back(vFloor2);
	room.vertices.push_back(vFloor3);
	room.vertices.push_back(vFloor4);
	
	
	vertex vCeiling1;
	vertex vCeiling2;
	vertex vCeiling3;
	vertex vCeiling4;

	vCeiling1.position = vFloor1.position + height;
	vCeiling2.position = vFloor2.position + height;
	vCeiling3.position = vFloor3.position + height;
	vCeiling4.position = vFloor4.position + height;

	vCeiling1.colour = glm::vec3(1.0f, 0.0f, 0.0f);
	vCeiling2.colour = glm::vec3(0.0f, 1.0f, 0.0f);
	vCeiling3.colour = glm::vec3(0.0f, 0.0f, 1.0f);
	vCeiling4.colour = glm::vec3(0.0f, 1.0f, 1.0f);

	vCeiling1.normal = glm::vec3(0.0f, -1.0f, 0.0f);
	vCeiling2.normal = glm::vec3(0.0f, -1.0f, 0.0f);
	vCeiling3.normal = glm::vec3(0.0f, -1.0f, 0.0f);
	vCeiling4.normal = glm::vec3(0.0f, -1.0f, 0.0f);

	room.vertices.push_back(vCeiling1);
	room.vertices.push_back(vCeiling2);
	room.vertices.push_back(vCeiling3);
	room.vertices.push_back(vCeiling4);
	
	//Floor
	room.indices.push_back(wallEnd);
	room.indices.push_back(wallEnd+1);
	room.indices.push_back(wallEnd+3);

	room.indices.push_back(wallEnd+1);
	room.indices.push_back(wallEnd+2);
	room.indices.push_back(wallEnd+3);

	//Ceiling
	room.indices.push_back(wallEnd+4);
	room.indices.push_back(wallEnd+7);
	room.indices.push_back(wallEnd+5);

	room.indices.push_back(wallEnd+5);
	room.indices.push_back(wallEnd+7);
	room.indices.push_back(wallEnd+6);

	room.vertices.shrink_to_fit();
	room.indices.shrink_to_fit();
	room.numIndices = room.indices.size();
	room.numVerts = room.vertices.size();

	return room;
}

GenerateShape::GenerateShape(void)
{
}


GenerateShape::~GenerateShape(void)
{
}

/*

OLD MAKESPHARE
shape GenerateShape::makeSphere(shape sphere, GLuint recursions, float radius)
{

	if(sphere.numVerts == 0)//AKA the shape is empty
		sphere = makeTetrahedron(radius);// Make the base; a tetrahedron.

	if(!recursions) //If there are no recursions to make left.
		return sphere;

	std::vector<GLuint> newIndices;
	int i;
	for (i = 0; i < sphere.numIndices; i+=3)
	{
		vertex oldVert1 = sphere.vertices[sphere.indices[i]];
		vertex oldVert2 = sphere.vertices[sphere.indices[i+1]];
		vertex oldVert3 = sphere.vertices[sphere.indices[i+2]];
		vertex newVert;

		glm::vec3 newPosition; //The centroid
		newPosition.x = (oldVert1.position.x + oldVert2.position.x + oldVert3.position.x);
		newPosition.y = (oldVert1.position.y + oldVert2.position.y + oldVert3.position.y);
		newPosition.z = (oldVert1.position.z + oldVert2.position.z + oldVert3.position.z);

		newPosition.x = newPosition.x / 3.0f;
		newPosition.y = newPosition.y / 3.0f;
		newPosition.z = newPosition.z / 3.0f;

		newPosition = glm::normalize(newPosition) * radius;
		newVert.position = newPosition;
		newVert.colour = glm::vec3(1, 1, 0); //Test colour
		printf("new x:%f, y:%f, z:%f \n", oldVert1.position.x, oldVert1.position.y, oldVert1.position.z);
		printf("old 1 x:%f, y:%f, z:%f \n", oldVert2.position.x, oldVert2.position.y, oldVert2.position.z);
		printf("old 2 x:%f, y:%f, z:%f \n", oldVert3.position.x, oldVert3.position.y, oldVert3.position.z);
		printf("old 3 x:%f, y:%f, z:%f \n\n", newPosition.x, newPosition.y, newPosition.z);
		sphere.vertices.push_back(newVert); //Add the vertex to the shapes list

		//Calculate the new vertex's index and add the indices for the three new triangles.
		GLuint newVertIndex = sphere.vertices.size()-1;
		//printf("New index:%d\n", newVertIndex);
		newIndices.push_back(sphere.indices[i]);
		newIndices.push_back(sphere.indices[i+1]);
		newIndices.push_back(newVertIndex);

		newIndices.push_back(sphere.indices[i+1]);
		newIndices.push_back(sphere.indices[i+2]);
		newIndices.push_back(newVertIndex);

		newIndices.push_back(sphere.indices[i+2]);
		newIndices.push_back(sphere.indices[i]);
		newIndices.push_back(newVertIndex);
	}

	//Set important data
	sphere.indices.resize(newIndices.size());
	sphere.indices.swap(newIndices);

	sphere.numVerts = sphere.vertices.size();
	sphere.numIndices = sphere.indices.size();

	return makeSphere(sphere, recursions-1, radius);
}

*/