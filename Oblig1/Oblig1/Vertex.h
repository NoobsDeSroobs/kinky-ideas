#pragma once
#include <glm/glm.hpp>

struct vertex{
public:
	glm::vec3 position;
	glm::vec3 colour;
	glm::vec3 normal;
	glm::vec2 UV;

	/*vertex(glm::vec3 position, glm::vec3 colour, glm::vec3 normal, glm::vec2 UV)
		:position(position), colour(colour), normal(normal), UV(UV){};

	vertex(){};*/
};