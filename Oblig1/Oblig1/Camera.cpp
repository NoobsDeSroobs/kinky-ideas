#include "Camera.h"

Camera::Camera(void) : position(0.0f, 0.0f, 0.0f), viewDir(0.0f, 0.0f, -1.0f), upVector(0.0f, 1.0f, 0.0f)
{
	MOVEMENT_SPEED = 0.1f;
	MOUSE_SPEED = 0.1f;
	SPEEDBOOSTER = 1.0f;
}

void Camera::update(const glm::vec2 &newMousePosition)
{
	glm::vec2 deltaMouse = (newMousePosition - center)*MOUSE_SPEED;
	viewDir = (glm::mat3(glm::rotate(-deltaMouse.x, upVector)) * 
		glm::mat3(glm::rotate(deltaMouse.y, glm::cross(upVector, viewDir)))) * viewDir;
	strafeDir = glm::cross(upVector, viewDir);
}

void Camera::moveForward()
{
	position += (MOVEMENT_SPEED*SPEEDBOOSTER) * viewDir;
}

void Camera::moveBackward()
{
	position -= (MOVEMENT_SPEED*SPEEDBOOSTER) * viewDir;
}

void Camera::strafeLeft()
{
	position += (MOVEMENT_SPEED*SPEEDBOOSTER) * strafeDir;
}

void Camera::strafeRight()
{
	position -= (MOVEMENT_SPEED*SPEEDBOOSTER) * strafeDir;
}

void Camera::ascend()
{
	position += (MOVEMENT_SPEED*SPEEDBOOSTER) * upVector;
}

void Camera::descend()
{
	position -= (MOVEMENT_SPEED*SPEEDBOOSTER) * upVector;
}

void Camera::setCenter(int width, int height) {
		center.x = (float)width/2;
		center.y = (float)height/2;
	};
glm::vec3 Camera::getPosition()
{
	return position;
}