#pragma once
#include "Shape.h"
#include "Camera.h"
#include <vector>

class Prop
{
	
public:
	shape Shape;
	std::vector<glm::mat4> matrices;
	Prop(void);
	void generateCubeTowerMatrices(GLuint numLevels, glm::vec3 start, Camera* camera, glm::mat4* projectionMatrix);
	void generateHollowCubeTowerMatrices(GLuint numLevels, glm::vec3 start, Camera* camera, glm::mat4* projectionMatrix);
	void setShape(shape newShape);
	shape gShape();

};

