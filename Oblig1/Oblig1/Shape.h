#pragma once
#include <GL/glew.h>
#include <memory>
#include "Vertex.h"
#include <vector>


struct shape{
public:
	shape():
		numVerts(0), numIndices(0), center(0.0f, 0.0f, 0.0f) {}
	shape(float x, float y, float z):
		numVerts(0), numIndices(0), center(x, y, z) {}

	std::vector<vertex> vertices; //Vertex list of this 3D shape
	GLuint numVerts;
	std::vector<GLuint> indices; //OpenGL indices.
	GLuint numIndices;	
	glm::vec3 center;
	//Enables us to easily get the buffer size required.
	GLsizeiptr vertexBufferSize() const
	{
		return numVerts * sizeof(vertex);
	}
	GLsizeiptr indexBufferSize() const
	{
		return numIndices * sizeof(GLuint);
	}
};