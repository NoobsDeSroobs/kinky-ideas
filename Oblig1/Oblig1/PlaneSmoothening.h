#pragma once

#include <vector>
#include <glm\glm.hpp>
 
std::vector<glm::vec3> smoothByAvarage(std::vector<glm::vec3> pointList);
std::vector<glm::vec3> smoothByAveragingBaseVector(std::vector<glm::vec3> pointList);